\name{experiment}
\alias{experiment}
\docType{data}
\title{
%%   ~~ data name/kind ... ~~
experiment
}
\description{
%%  ~~ A concise (1-5 lines) description of the dataset. ~~
}
\usage{data(experiment)}
\format{
  The format is:
Reference class 'refObjectGenerator' [package "methods"] with 2 fields
 $ def      :Formal class 'refClassRepresentation' [package "methods"] with 15 slots
  .. ..@ fieldClasses   :List of 13
  .. .. ..$ db        : chr "character"
  .. .. ..$ connector : chr "mongo"
  .. .. ..$ population: chr "character"
  .. .. ..$ model     : chr "character"
  .. .. ..$ oid       : chr "mongo.oid"
  .. .. ..$ content   : chr "list"
  .. .. ..$ is.saved  : chr "logical"
  .. .. ..$ parents   : chr "list"
  .. .. ..$ fields    : chr "list"
  .. .. ..$ nuclei    : chr "list"
  .. .. ..$ selections: chr "list"
  .. .. ..$ metaData  : chr "list"
  .. .. ..$ is.fetched: chr "logical"
  .. ..@ fieldPrototypes:<environment: 0x9a5a860> 
  .. ..@ refMethods     :<environment: 0x64e9f88> 
  .. ..@ refSuperClasses: chr [1:2] "mongoDbPersistentObject" "envRefClass"
  .. ..@ slots          :List of 1
  .. .. ..$ .xData: atomic [1:1] environment
  .. .. .. ..- attr(*, "package")= chr "methods"
  .. ..@ contains       :List of 6
  .. .. ..$ mongoDbPersistentObject:Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "experiment"
  .. .. .. .. ..@ superClass: chr "mongoDbPersistentObject"
  .. .. .. .. ..@ package   : chr "rtango"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi TRUE
  .. .. .. .. ..@ by        : chr(0) 
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 1
  .. .. ..$ envRefClass            :Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "experiment"
  .. .. .. .. ..@ superClass: chr "envRefClass"
  .. .. .. .. ..@ package   : chr "rmpo"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi TRUE
  .. .. .. .. ..@ by        : chr "mongoDbPersistentObject"
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 2
  .. .. ..$ .environment           :Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "experiment"
  .. .. .. .. ..@ superClass: chr ".environment"
  .. .. .. .. ..@ package   : chr "methods"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi TRUE
  .. .. .. .. ..@ by        : chr "mongoDbPersistentObject"
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 3
  .. .. ..$ refClass               :Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "experiment"
  .. .. .. .. ..@ superClass: chr "refClass"
  .. .. .. .. ..@ package   : chr "methods"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi TRUE
  .. .. .. .. ..@ by        : chr "mongoDbPersistentObject"
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 3
  .. .. ..$ environment            :Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "experiment"
  .. .. .. .. ..@ superClass: chr "environment"
  .. .. .. .. ..@ package   : chr "methods"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi FALSE
  .. .. .. .. ..@ by        : chr "mongoDbPersistentObject"
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 4
  .. .. ..$ refObject              :Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "experiment"
  .. .. .. .. ..@ superClass: chr "refObject"
  .. .. .. .. ..@ package   : chr "methods"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi TRUE
  .. .. .. .. ..@ by        : chr "mongoDbPersistentObject"
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 4
  .. ..@ virtual        : logi FALSE
  .. ..@ prototype      :Formal class 'S4' [package ""] with 0 slots
 list()
  .. ..@ validity       : NULL
  .. ..@ access         : list()
  .. ..@ className      : atomic [1:1] experiment
  .. .. ..- attr(*, "package")= chr "rtango"
  .. ..@ package        : chr "rtango"
  .. ..@ subclasses     : list()
  .. ..@ versionKey     :<externalptr> 
  .. ..@ sealed         : logi FALSE
 $ className: chr "experiment"
 and 18 methods, of which 6 are possibly relevant:
   accessors, fields, help, lock, methods, new
}
\details{
%%  ~~ If necessary, more details than the __description__ above ~~
}
\source{
%%  ~~ reference to a publication or URL from which the data were obtained ~~
}
\references{
%%  ~~ possibly secondary sources and usages ~~
}
\examples{
data(experiment)
## maybe str(experiment) ; plot(experiment) ...
}
\keyword{datasets}
