\name{tangoTask}
\alias{tangoTask}
\docType{data}
\title{
tangoTask
}
\description{
%%  ~~ A concise (1-5 lines) description of the dataset. ~~
}
\usage{data(tangoTask)}
\format{
  The format is:
Reference class 'refObjectGenerator' [package "methods"] with 2 fields
 $ def      :Formal class 'refClassRepresentation' [package "methods"] with 15 slots
  .. ..@ fieldClasses   :List of 6
  .. .. ..$ name       : chr "character"
  .. .. ..$ description: chr "character"
  .. .. ..$ type       : chr "character"
  .. .. ..$ structures : chr "character"
  .. .. ..$ filters    : chr "character"
  .. .. ..$ workspaces : chr "character"
  .. ..@ fieldPrototypes:<environment: 0x4b9f070> 
  .. ..@ refMethods     :<environment: 0x4b9ffb8> 
  .. ..@ refSuperClasses: chr [1:2] "tangoConfigData" "envRefClass"
  .. ..@ slots          :List of 1
  .. .. ..$ .xData: atomic [1:1] environment
  .. .. .. ..- attr(*, "package")= chr "methods"
  .. ..@ contains       :List of 6
  .. .. ..$ tangoConfigData:Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "tangoTask"
  .. .. .. .. ..@ superClass: chr "tangoConfigData"
  .. .. .. .. ..@ package   : chr "rtango"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi TRUE
  .. .. .. .. ..@ by        : chr(0) 
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 1
  .. .. ..$ envRefClass    :Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "tangoTask"
  .. .. .. .. ..@ superClass: chr "envRefClass"
  .. .. .. .. ..@ package   : chr "rtango"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi TRUE
  .. .. .. .. ..@ by        : chr "tangoConfigData"
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 2
  .. .. ..$ .environment   :Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "tangoTask"
  .. .. .. .. ..@ superClass: chr ".environment"
  .. .. .. .. ..@ package   : chr "methods"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi TRUE
  .. .. .. .. ..@ by        : chr "tangoConfigData"
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 3
  .. .. ..$ refClass       :Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "tangoTask"
  .. .. .. .. ..@ superClass: chr "refClass"
  .. .. .. .. ..@ package   : chr "methods"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi TRUE
  .. .. .. .. ..@ by        : chr "tangoConfigData"
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 3
  .. .. ..$ environment    :Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "tangoTask"
  .. .. .. .. ..@ superClass: chr "environment"
  .. .. .. .. ..@ package   : chr "methods"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi FALSE
  .. .. .. .. ..@ by        : chr "tangoConfigData"
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 4
  .. .. ..$ refObject      :Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "tangoTask"
  .. .. .. .. ..@ superClass: chr "refObject"
  .. .. .. .. ..@ package   : chr "methods"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi TRUE
  .. .. .. .. ..@ by        : chr "tangoConfigData"
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 4
  .. ..@ virtual        : logi FALSE
  .. ..@ prototype      :Formal class 'S4' [package ""] with 0 slots
 list()
  .. ..@ validity       : NULL
  .. ..@ access         : list()
  .. ..@ className      : atomic [1:1] tangoTask
  .. .. ..- attr(*, "package")= chr "rtango"
  .. ..@ package        : chr "rtango"
  .. ..@ subclasses     : list()
  .. ..@ versionKey     :<externalptr> 
  .. ..@ sealed         : logi FALSE
 $ className: chr "tangoTask"
 and 18 methods, of which 6 are possibly relevant:
   accessors, fields, help, lock, methods, new
}
\details{
%%  ~~ If necessary, more details than the __description__ above ~~
}
\source{
%%  ~~ reference to a publication or URL from which the data were obtained ~~
}
\references{
%%  ~~ possibly secondary sources and usages ~~
}
\examples{
data(tangoTask)
## maybe str(tangoTask) ; plot(tangoTask) ...
}
\keyword{datasets}
