\name{experiments-class}
\Rdversion{1.1}
\docType{class}
\alias{experiments-class}

\title{Class \code{"experiments"}}
\description{
%%  ~~ A concise (1-5 lines) description of what the class is. ~~
}
\section{Extends}{
Class \code{"\linkS4class{mongoDbPersistentPopulation}"}, directly.

All reference classes extend and inherit methods from \code{"\linkS4class{envRefClass}"}.

}
\references{
%%  ~~put references to the literature/web site here~~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%%  ~~objects to See Also as \code{\link{~~fun~~}}, ~~~
%%  ~~or \code{\linkS4class{CLASSNAME}} for links to other classes ~~~
}
\examples{
showClass("experiments")
}
\keyword{classes}
\section{Fields}{
  \describe{
    \item{\code{connector}:}{Object of class \code{mongo} ~~ }
    \item{\code{db}:}{Object of class \code{character} ~~ }
    \item{\code{population}:}{Object of class \code{character} ~~ }
    \item{\code{model}:}{Object of class \code{character} ~~ }
    \item{\code{query}:}{Object of class \code{mongo.bson} ~~ }
    \item{\code{sort}:}{Object of class \code{mongo.bson} ~~ }
    \item{\code{queryfields}:}{Object of class \code{mongo.bson} ~~ }
    \item{\code{parents}:}{Object of class \code{list} ~~ }
    \item{\code{elements}:}{Object of class \code{list} ~~ }
    \item{\code{filter}:}{Object of class \code{list} ~~ }
    \item{\code{exclude}:}{Object of class \code{list} ~~ }
  }
}
\section{Methods}{
  \describe{
    \item{\code{setPopulation()}:}{ ~~ }
    \item{\code{setModel()}:}{ ~~ }
  }

The following methods are inherited (from the corresponding class):
setModel ("mongoDbPersistentPopulation"), setPopulation ("mongoDbPersistentPopulation"), countElements ("mongoDbPersistentPopulation"), initFields ("mongoDbPersistentPopulation"), indexBy ("mongoDbPersistentPopulation"), fetch ("mongoDbPersistentPopulation"), filter ("mongoDbPersistentPopulation"), getCursor ("mongoDbPersistentPopulation"), getNameSpace ("mongoDbPersistentPopulation")
}
