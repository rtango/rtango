\name{tangoCriteriaFilter}
\alias{tangoCriteriaFilter}
\docType{data}
\title{
tangoCriteriaFilter
}
\description{
%%  ~~ A concise (1-5 lines) description of the dataset. ~~
}
\usage{data(tangoCriteriaFilter)}
\format{
  The format is:
Reference class 'refObjectGenerator' [package "methods"] with 2 fields
 $ def      :Formal class 'refClassRepresentation' [package "methods"] with 15 slots
  .. ..@ fieldClasses   :List of 1
  .. .. ..$ criteria: chr "character"
  .. ..@ fieldPrototypes:<environment: 0x4965180> 
  .. ..@ refMethods     :<environment: 0x4a84f80> 
  .. ..@ refSuperClasses: chr "envRefClass"
  .. ..@ slots          :List of 1
  .. .. ..$ .xData: atomic [1:1] environment
  .. .. .. ..- attr(*, "package")= chr "methods"
  .. ..@ contains       :List of 5
  .. .. ..$ envRefClass :Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "tangoCriteriaFilter"
  .. .. .. .. ..@ superClass: chr "envRefClass"
  .. .. .. .. ..@ package   : chr "rtango"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi TRUE
  .. .. .. .. ..@ by        : chr(0) 
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 1
  .. .. ..$ .environment:Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "tangoCriteriaFilter"
  .. .. .. .. ..@ superClass: chr ".environment"
  .. .. .. .. ..@ package   : chr "methods"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi TRUE
  .. .. .. .. ..@ by        : chr "envRefClass"
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 2
  .. .. ..$ refClass    :Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "tangoCriteriaFilter"
  .. .. .. .. ..@ superClass: chr "refClass"
  .. .. .. .. ..@ package   : chr "methods"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi TRUE
  .. .. .. .. ..@ by        : chr "envRefClass"
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 2
  .. .. ..$ environment :Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "tangoCriteriaFilter"
  .. .. .. .. ..@ superClass: chr "environment"
  .. .. .. .. ..@ package   : chr "methods"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi FALSE
  .. .. .. .. ..@ by        : chr "envRefClass"
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 3
  .. .. ..$ refObject   :Formal class 'SClassExtension' [package "methods"] with 10 slots
  .. .. .. .. ..@ subClass  : chr "tangoCriteriaFilter"
  .. .. .. .. ..@ superClass: chr "refObject"
  .. .. .. .. ..@ package   : chr "methods"
  .. .. .. .. ..@ coerce    :function (from, strict = TRUE)  
  .. .. .. .. ..@ test      :function (object)  
  .. .. .. .. ..@ replace   :function (from, to, value)  
  .. .. .. .. ..@ simple    : logi TRUE
  .. .. .. .. ..@ by        : atomic [1:1] envRefClass
  .. .. .. .. .. ..- attr(*, "package")= chr "methods"
  .. .. .. .. ..@ dataPart  : logi FALSE
  .. .. .. .. ..@ distance  : num 3
  .. ..@ virtual        : logi FALSE
  .. ..@ prototype      :Formal class 'S4' [package ""] with 0 slots
 list()
  .. ..@ validity       : NULL
  .. ..@ access         : list()
  .. ..@ className      : atomic [1:1] tangoCriteriaFilter
  .. .. ..- attr(*, "package")= chr "rtango"
  .. ..@ package        : chr "rtango"
  .. ..@ subclasses     : list()
  .. ..@ versionKey     :<externalptr> 
  .. ..@ sealed         : logi FALSE
 $ className: chr "tangoCriteriaFilter"
 and 18 methods, of which 6 are possibly relevant:
   accessors, fields, help, lock, methods, new
}
\details{
%%  ~~ If necessary, more details than the __description__ above ~~
}
\source{
%%  ~~ reference to a publication or URL from which the data were obtained ~~
}
\references{
%%  ~~ possibly secondary sources and usages ~~
}
\examples{
data(tangoCriteriaFilter)
## maybe str(tangoCriteriaFilter) ; plot(tangoCriteriaFilter) ...
}
\keyword{datasets}
