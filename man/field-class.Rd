\name{field-class}
\Rdversion{1.1}
\docType{class}
\alias{field-class}

\title{Class \code{"field"}}
\description{
%%  ~~ A concise (1-5 lines) description of what the class is. ~~
}
\section{Extends}{
Class \code{"\linkS4class{mongoDbPersistentObject}"}, directly.

All reference classes extend and inherit methods from \code{"\linkS4class{envRefClass}"}.

}
\references{
%%  ~~put references to the literature/web site here~~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%%  ~~objects to See Also as \code{\link{~~fun~~}}, ~~~
%%  ~~or \code{\linkS4class{CLASSNAME}} for links to other classes ~~~
}
\examples{
showClass("field")
}
\keyword{classes}
\section{Fields}{
  \describe{
    \item{\code{db}:}{Object of class \code{character} ~~ }
    \item{\code{connector}:}{Object of class \code{mongo} ~~ }
    \item{\code{population}:}{Object of class \code{character} ~~ }
    \item{\code{model}:}{Object of class \code{character} ~~ }
    \item{\code{oid}:}{Object of class \code{mongo.oid} ~~ }
    \item{\code{content}:}{Object of class \code{list} ~~ }
    \item{\code{is.saved}:}{Object of class \code{logical} ~~ }
    \item{\code{parents}:}{Object of class \code{list} ~~ }
  }
}
\section{Methods}{
  \describe{
    \item{\code{getNuclei()}:}{ ~~ }
    \item{\code{setPopulation()}:}{ ~~ }
    \item{\code{setModel()}:}{ ~~ }
  }

The following methods are inherited (from the corresponding class):
setModel ("mongoDbPersistentObject"), setPopulation ("mongoDbPersistentObject"), save ("mongoDbPersistentObject"), getForeignQuery ("mongoDbPersistentObject"), getId ("mongoDbPersistentObject"), initFields ("mongoDbPersistentObject"), fetch ("mongoDbPersistentObject"), getParent ("mongoDbPersistentObject"), getContent ("mongoDbPersistentObject"), getNameSpace ("mongoDbPersistentObject"), update ("mongoDbPersistentObject"), insert ("mongoDbPersistentObject"), getRelateds ("mongoDbPersistentObject"), getSelfQuery ("mongoDbPersistentObject"), postInitialize ("mongoDbPersistentObject"), getAttribute ("mongoDbPersistentObject")
}
