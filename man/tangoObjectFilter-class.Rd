\name{tangoObjectFilter-class}
\Rdversion{1.1}
\docType{class}
\alias{tangoObjectFilter-class}

\title{Class \code{"tangoObjectFilter"}}
\description{
%%  ~~ A concise (1-5 lines) description of what the class is. ~~
}
\section{Extends}{

All reference classes extend and inherit methods from \code{"\linkS4class{envRefClass}"}.

}
\references{
%%  ~~put references to the literature/web site here~~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%%  ~~objects to See Also as \code{\link{~~fun~~}}, ~~~
%%  ~~or \code{\linkS4class{CLASSNAME}} for links to other classes ~~~
}
\examples{
showClass("tangoObjectFilter")
}
\keyword{classes}
\section{Fields}{
  \describe{
    \item{\code{structures}:}{Object of class \code{character} ~~ }
    \item{\code{constraint}:}{Object of class \code{character} ~~ }
  }
}
\section{Methods}{
  \describe{
    \item{\code{getNucleiIdsList(experiment)}:}{ ~~ }
  }
}
