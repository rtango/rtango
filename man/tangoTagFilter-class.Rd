\name{tangoTagFilter-class}
\Rdversion{1.1}
\docType{class}
\alias{tangoTagFilter-class}

\title{Class \code{"tangoTagFilter"}}
\description{
%%  ~~ A concise (1-5 lines) description of what the class is. ~~
}
\section{Extends}{

All reference classes extend and inherit methods from \code{"\linkS4class{envRefClass}"}.

}
\references{
%%  ~~put references to the literature/web site here~~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%%  ~~objects to See Also as \code{\link{~~fun~~}}, ~~~
%%  ~~or \code{\linkS4class{CLASSNAME}} for links to other classes ~~~
}
\examples{
showClass("tangoTagFilter")
}
\keyword{classes}
\section{Fields}{
  \describe{
    \item{\code{exclude}:}{Object of class \code{integer} ~~ }
  }
}
\section{Methods}{
  \describe{
    \item{\code{apply(experiment)}:}{ ~~ }
  }
}
