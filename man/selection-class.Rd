\name{selection-class}
\Rdversion{1.1}
\docType{class}
\alias{selection-class}

\title{Class \code{"selection"}}
\description{
%%  ~~ A concise (1-5 lines) description of what the class is. ~~
}
\section{Extends}{
Class \code{"\linkS4class{mongoDbPersistentObject}"}, directly.

All reference classes extend and inherit methods from \code{"\linkS4class{envRefClass}"}.

}
\references{
%%  ~~put references to the literature/web site here~~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%%  ~~objects to See Also as \code{\link{~~fun~~}}, ~~~
%%  ~~or \code{\linkS4class{CLASSNAME}} for links to other classes ~~~
}
\examples{
showClass("selection")
}
\keyword{classes}
\section{Fields}{
  \describe{
    \item{\code{db}:}{Object of class \code{character} ~~ }
    \item{\code{connector}:}{Object of class \code{mongo} ~~ }
    \item{\code{population}:}{Object of class \code{character} ~~ }
    \item{\code{model}:}{Object of class \code{character} ~~ }
    \item{\code{oid}:}{Object of class \code{mongo.oid} ~~ }
    \item{\code{content}:}{Object of class \code{list} ~~ }
    \item{\code{is.saved}:}{Object of class \code{logical} ~~ }
    \item{\code{parents}:}{Object of class \code{list} ~~ }
  }
}
\section{Methods}{
  \describe{
    \item{\code{johnnyWeirdFunction(s1, s2, n1, n2, tags)}:}{ ~~ }
    \item{\code{filter(df)}:}{ ~~ }
    \item{\code{setdiffSelection(s)}:}{ ~~ }
    \item{\code{intersectSelection(s)}:}{ ~~ }
    \item{\code{unionSelection(s)}:}{ ~~ }
    \item{\code{toDataFrame()}:}{ ~~ }
    \item{\code{getUniqueName()}:}{ ~~ }
    \item{\code{checkUniqueName(name, siblingsNames)}:}{ ~~ }
    \item{\code{getSiblingsNames()}:}{ ~~ }
    \item{\code{overwriteSiblings()}:}{ ~~ }
    \item{\code{cleanSiblings()}:}{ ~~ }
    \item{\code{getKeys()}:}{ ~~ }
    \item{\code{getStructures()}:}{ ~~ }
    \item{\code{getExperiment()}:}{ ~~ }
    \item{\code{save(override)}:}{ ~~ }
    \item{\code{setPopulation()}:}{ ~~ }
    \item{\code{setModel()}:}{ ~~ }
  }

The following methods are inherited (from the corresponding class):
save ("mongoDbPersistentObject"), setModel ("mongoDbPersistentObject"), setPopulation ("mongoDbPersistentObject"), getForeignQuery ("mongoDbPersistentObject"), getId ("mongoDbPersistentObject"), initFields ("mongoDbPersistentObject"), fetch ("mongoDbPersistentObject"), getParent ("mongoDbPersistentObject"), getContent ("mongoDbPersistentObject"), getNameSpace ("mongoDbPersistentObject"), update ("mongoDbPersistentObject"), insert ("mongoDbPersistentObject"), getRelateds ("mongoDbPersistentObject"), getSelfQuery ("mongoDbPersistentObject"), postInitialize ("mongoDbPersistentObject"), getAttribute ("mongoDbPersistentObject")
}
