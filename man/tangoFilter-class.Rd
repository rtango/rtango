\name{tangoFilter-class}
\Rdversion{1.1}
\docType{class}
\alias{tangoFilter-class}

\title{Class \code{"tangoFilter"}}
\description{
%%  ~~ A concise (1-5 lines) description of what the class is. ~~
}
\section{Extends}{
Class \code{"\linkS4class{tangoConfigData}"}, directly.

All reference classes extend and inherit methods from \code{"\linkS4class{envRefClass}"}.

}
\references{
%%  ~~put references to the literature/web site here~~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%%  ~~objects to See Also as \code{\link{~~fun~~}}, ~~~
%%  ~~or \code{\linkS4class{CLASSNAME}} for links to other classes ~~~
}
\examples{
showClass("tangoFilter")
}
\keyword{classes}
\section{Fields}{
  \describe{
    \item{\code{name}:}{Object of class \code{character} ~~ }
    \item{\code{description}:}{Object of class \code{character} ~~ }
    \item{\code{type}:}{Object of class \code{character} ~~ }
    \item{\code{subtype}:}{Object of class \code{character} ~~ }
  }
}
\section{Methods}{

The following methods are inherited (from the corresponding class):
save ("tangoConfigData")
}
