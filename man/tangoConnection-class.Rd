\name{tangoConnection-class}
\Rdversion{1.1}
\docType{class}
\alias{tangoConnection-class}

\title{Class \code{"tangoConnection"}}
\description{
%%  ~~ A concise (1-5 lines) description of what the class is. ~~
}
\section{Extends}{
Class \code{"\linkS4class{mongoDbConnection}"}, directly.

All reference classes extend and inherit methods from \code{"\linkS4class{envRefClass}"}.

}
\references{
%%  ~~put references to the literature/web site here~~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%%  ~~objects to See Also as \code{\link{~~fun~~}}, ~~~
%%  ~~or \code{\linkS4class{CLASSNAME}} for links to other classes ~~~
}
\examples{
showClass("tangoConnection")
}
\keyword{classes}
\section{Fields}{
  \describe{
    \item{\code{auth}:}{Object of class \code{logical} ~~ }
    \item{\code{host}:}{Object of class \code{character} ~~ }
    \item{\code{username}:}{Object of class \code{character} ~~ }
    \item{\code{password}:}{Object of class \code{character} ~~ }
    \item{\code{db}:}{Object of class \code{character} ~~ }
    \item{\code{connector}:}{Object of class \code{mongo} ~~ }
    \item{\code{projects}:}{Object of class \code{list} ~~ }
  }
}
\section{Methods}{
  \describe{
    \item{\code{findInAllTangoDatabases(collection, query, sort, queryfields)}:}{ ~~ }
    \item{\code{getTangoDatabases()}:}{ ~~ }
    \item{\code{extractSelections(df, name, description)}:}{ ~~ }
    \item{\code{getSettingsConnector()}:}{ ~~ }
    \item{\code{getSettingsDatabaseName()}:}{ ~~ }
    \item{\code{getExperimentsByName()}:}{ ~~ }
    \item{\code{getExperiments()}:}{ ~~ }
    \item{\code{extractObjectData(structures, keys, addIdx)}:}{ ~~ }
    \item{\code{extractStructureData(structures, keys, addIdx)}:}{ ~~ }
    \item{\code{getProjects()}:}{ ~~ }
    \item{\code{postInitialize()}:}{ ~~ }
  }

The following methods are inherited (from the corresponding class):
getParameters ("mongoDbConnection"), initFields ("mongoDbConnection"), find ("mongoDbConnection"), switchDb ("mongoDbConnection"), getDatabases ("mongoDbConnection")
}
