\name{experiment-class}
\Rdversion{1.1}
\docType{class}
\alias{experiment-class}

\title{Class \code{"experiment"}}
\description{
This class handles the results of a 3D microscope experiment about nuclear genome organisation.
It stores its data in a MongoDB database.
}
\section{Extends}{
Class \code{"\linkS4class{mongoDbPersistentObject}"}, directly.

All reference classes extend and inherit methods from \code{"\linkS4class{envRefClass}"}.

}
\references{
http://biophysique.mnhn.fr/tango/Getting+Started
}
\author{
Jean Ollion
Julien Cochennec
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
\code{\linkS4class{nucleus}}
}
\examples{
showClass("experiment")
}
\keyword{classes}
\section{Fields}{
  \describe{
    \item{\code{db}:}{Object of class \code{character} ~~ }
    \item{\code{connector}:}{Object of class \code{mongo} ~~ }
    \item{\code{population}:}{Object of class \code{character} ~~ }
    \item{\code{model}:}{Object of class \code{character} ~~ }
    \item{\code{oid}:}{Object of class \code{mongo.oid} ~~ }
    \item{\code{content}:}{Object of class \code{list} ~~ }
    \item{\code{is.saved}:}{Object of class \code{logical} ~~ }
    \item{\code{parents}:}{Object of class \code{list} ~~ }
    \item{\code{fields}:}{Object of class \code{list} ~~ }
    \item{\code{nuclei}:}{Object of class \code{list} ~~ }
    \item{\code{selections}:}{Object of class \code{list} ~~ }
    \item{\code{metaData}:}{Object of class \code{list} ~~ }
    \item{\code{is.fetched}:}{Object of class \code{logical} ~~ }
  }
}
\section{Methods}{
  \describe{
    \item{\code{getFilter(s1, n1, s2, n2, tags)}:}{ ~~ }
    \item{\code{getTagsAndStructures(structures)}:}{ ~~ }
    \item{\code{getObjectsCount(structure, filter)}:}{ ~~ }
    \item{\code{extractObjectData(structure, keys, filter, addIdx, useMetaData)}:}{ ~~ }
    \item{\code{extractStructureData(structures, keys, filter, addIdx, useMetaData)}:}{ ~~ }
    \item{\code{getKey(structures)}:}{ ~~ }
    \item{\code{setFields()}:}{ ~~ }
    \item{\code{getNucleiFieldsInformation()}:}{ ~~ }
    \item{\code{createEmptySelection()}:}{ ~~ }
    \item{\code{createWholeSelection()}:}{ ~~ }
    \item{\code{setSelections()}:}{ ~~ }
    \item{\code{setNuclei()}:}{ ~~ }
    \item{\code{getTags(filter)}:}{ ~~ }
    \item{\code{getAllStructuresNames()}:}{ ~~ }
    \item{\code{extractSelection(df, selectionName, description, columnNames)}:}{ ~~ }
    \item{\code{extractObjectIndexes(df, columnNames)}:}{ ~~ }
    \item{\code{setMetaData(data)}:}{ ~~ }
    \item{\code{getAllStructures()}:}{ ~~ }
    \item{\code{getVirtualStructuresNames()}:}{ ~~ }
    \item{\code{getVirtualStructuresByName()}:}{ ~~ }
    \item{\code{getStructuresNames()}:}{ ~~ }
    \item{\code{getStructuresByName()}:}{ ~~ }
    \item{\code{getStructures()}:}{ ~~ }
    \item{\code{fetchData()}:}{ ~~ }
    \item{\code{show()}:}{ ~~ }
    \item{\code{setPopulation()}:}{ ~~ }
    \item{\code{setModel()}:}{ ~~ }
    \item{\code{postInitialize()}:}{ ~~ }
  }

The following methods are inherited (from the corresponding class):
postInitialize ("mongoDbPersistentObject"), setModel ("mongoDbPersistentObject"), setPopulation ("mongoDbPersistentObject"), save ("mongoDbPersistentObject"), getForeignQuery ("mongoDbPersistentObject"), getId ("mongoDbPersistentObject"), initFields ("mongoDbPersistentObject"), fetch ("mongoDbPersistentObject"), getParent ("mongoDbPersistentObject"), getContent ("mongoDbPersistentObject"), getNameSpace ("mongoDbPersistentObject"), update ("mongoDbPersistentObject"), insert ("mongoDbPersistentObject"), getRelateds ("mongoDbPersistentObject"), getSelfQuery ("mongoDbPersistentObject"), getAttribute ("mongoDbPersistentObject")
}
