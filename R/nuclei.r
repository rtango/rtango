nuclei = setRefClass("nuclei",
  contains="mongoDbPersistentPopulation",
  methods = list(
    setPopulation = function(){
      population <<- "nuclei"
    },
    setModel = function(){
      model <<- "nucleus"
    }
  )
)