tangoWorkFlow = setRefClass("tangoWorkFlow",
  fields = list(
    dataframes = "list"
  ),
  methods = list(
    toFlowFrame = function(df){
      ndf = df[sapply(df, is.numeric)]
      e = data.matrix(ndf)
      pdf = data.frame(name=names(ndf),row.names=paste('$P',1:length(names(ndf)),sep=''))
      padf = new("AnnotatedDataFrame", data=pdf)
      ff = new("flowFrame",exprs = e, parameters = padf)
      return(ff)
    },
    toFlowSet = function(){
      fflist = list()
      for(name in names(dflist)){
        fflist[[name]] = .self$toFlowFrame(dflist[[name]])
      }
      return(as(fflist,"flowSet"))
    },
    initialize = function(){
      assign("tangoFlowSet",.self$toFlowSet(),envir=.globalEnv)
    },
    getWorkFlow = function(){
      return(get(x='wf',envir=.guiEnv))
    },
    getViewNames = function(){
      return(names(.self$getWorkFlow()))
    },
    getView = function(viewName){
      return(.self$getWorkFlow()[[viewName]])
    },
    getIndices = function(viewName){
      v = .self$getView(viewName)
      return(attributes(v)$indices)
    },
    getSubset = function(dfName,viewName){
      return(.self$getSubsets()[[dfName]][[viewName]])
    },
    getSubsets = function(){
      subsets = list()
      wf = .self$getWorkFlow()
      for(dfName in names(dataframes)){
        subsets[[dfName]] = list()
        for(viewName in wf){
          v = wf[[viewName]]
          indices = attributes(v)$indices[[dfName]]
          subsets[[dfName]][[viewName]] = dataframes[[dfName]][indices,]
        }
      }
      return(subsets)
    }
  )
)