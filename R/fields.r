fields = setRefClass("fields",
  contains="mongoDbPersistentPopulation",
  fields = list(
    nuclei = "list",
    fields = "list"
  ),
  methods = list(
    setPopulation = function(){
      population <<- "experiments"
    },
    setModel = function(){
      model <<- "field"
    }
  )
)