experiment = setRefClass("experiment",
  fields = list(
    fields="list",
    nuclei="list",
	  selections="list",
	  metaData="list",
    is.fetched = "logical"
  ),
  methods = list(
    show = function(){
      print(content['name'])
    },
    setPopulation = function(){
      population <<- "experiments"
    },
    setModel = function(){
      model <<- "experiment"
    },
    postInitialize = function(){
      is.fetched <<- FALSE
    },
    fetchData = function(){
      if(!is.fetched){
        .self$setNuclei()
        .self$setFields()
        .self$setSelections()
        .self$setMetaData()
        is.fetched <<- TRUE
      }
    },
    getStructures = function(){
      l = .self$getContent("structures")
      names(l) = 0:(length(l)-1)
      return(l)
    },
    getVirtualStructures = function(){
      l = .self$getContent("virtualStructures")
      if(is.null(l) || length(l)==0) return(list())
      else{
        offset = length(.self$getStructures())
        names(l)=offset:(offset+length(l)-1)
        return(l)
      }
    },
    getStructuresByName = function(){
      structures=list()
      for(structure in .self$getStructures()){
          structures[[structure$name]] = structure
      }
      return(structures)
    },
    getStructuresNames = function(){
      return(names(.self$getStructuresByName()))
    },
#     getStructure = function(tag){
#       
#     },
#     getStructures = function(tags){
#       
#     },
    getVirtualStructuresByName = function(){
      virtualstructures=list()
      for(virtualstructure in .self$getVirtualStructures()){
        virtualstructures[[virtualstructure$name]] = virtualstructure
      }
      return(virtualstructures)
    },
    getVirtualStructuresNames = function(){
      return(names(.self$getVirtualStructuresByName))
    },
    getAllStructures = function(){
      return(c(.self$getStructures(),.self$getVirtualStructures()))
    },
	  setMetaData = function(data=NULL){
		  if(!is.null(data)){
			  for(name in names(data)){
				  metaData[[name]] <<- as.integer(data[[name]])
			  }
		  }
		  else{
			  s = .self$getAllStructures()
			  for(idx in names(s)){
				  metaData[[s[[idx]]$name]] <<- as.integer(idx)
			  }
		  }
	  },
    extractObjectIndexes = function(df, columnNames){ 
      ObjectIndexesByStructureIndex = list()
      for (name in names(columnNames)){
        structureIndex = columnNames[[name]]
        structureColumnValues = na.omit(unique(df[[name]])) 
        ObjectIndexesByStructureIndex[[structureIndex]] = unique(c(ObjectIndexesByStructureIndex[[structureIndex]],structureColumnValues))
      }
      return(ObjectIndexesByStructureIndex)
    },
    extractSelection = function(df,selectionName='currentSelection',description=NULL, columnNames=NULL,save=TRUE){
      if (is.null(columnNames)) {
        for (column.name in names(df)){
          structureName = strsplit(column.name,'.idx')[[1]]
          if(nchar(structureName)<nchar(column.name)){ # si le nom est plus court c'est qu'il a trouve .idx donc c'est un nom de colonne 
            structureIndex = toString(metaData[[structureName]])
            columnNames[[column.name]]=structureIndex
          }
        }
      } else { # correction au cas ou l'utilisateur ai donne des indices numeriques et non des strings
        for (column.name in names(columnNames)) {
          columnNames[[column.name]]=toString(columnNames[[column.name]])
        }
      }
      l = list()
      splitDfByNuclei = split(df, f = df$nucleus.id)
      for(nucleusId in names(splitDfByNuclei)){
        l[[nucleusId]] = .self$extractObjectIndexes(splitDfByNuclei[[nucleusId]], columnNames)
      }
      c = list(experiment_id=oid,name=selectionName,nuclei=l)
      sel=selection$new(connector=connector,content=c,parents=list(experiment = .self),db=db)
      sel$initFields()
      if(save){
        sel$save()
      }
      return(sel)
    },
    getAllStructuresNames = function(){
      return(c(.self$getStructuresNames(),.self$getVirtualStructuresNames()))
    },
    getTags = function(filter=NULL){
      if (is.null(filter)) {
        l = names(.self$nuclei)
      } else {
        l = filter
      }
      tags=numeric(0)
      for (id in l) {
        t=.self$nuclei[[id]]$getContent("tag")   
        if (!is.null(t)) tags[id]=t
        else  tags[id]=0
      }
      return(tags)
    },
    setNuclei = function(){
      ns = .self$getRelateds("nuclei")
      nuclei <<- ns$elements
    },
	  setSelections = function(){
		  s = .self$getRelateds("selections")
		  selections <<- s$indexBy("name")
	  },
    createWholeSelection = function(){
      l = list(experiment_id = oid, name = 'Whole',description='',nuclei = list())
      c = .self$getObjectsCount()
      nuclei.ids = names(nuclei)
      for(nucleus.id in nuclei.ids){
        l[['nuclei']][[nucleus.id]] = list()
        nucleus.structures.indexes = names(.self$getStructures())
        for(structure.index in nucleus.structures.indexes){
          nucleus = .self$nuclei[[nucleus.id]]
          nucleus$objects3dCount[[structure.index+1]]
          l[['nuclei']][[nucleus.id]][[structure.index]] = c(1:nucleus$objects3dCount[[structure.index+1]])
        }
      }
      #TODO construction de l'objet selection associé à partir des indexes
      return(selection$new(connector = connector,content = l,parents = parents,db = db))
    },
    createEmptySelection = function(){
      return(selection$new(connector = connector,content = list(experiment_id = oid, name = 'Empty',description='',nuclei = list()),parents = parents,db = db))
    },
    getNucleiFieldsInformation = function(){
      l = list()
      for(nucleus_id in names(exp$nuclei)){
        l[[nucleus_id]] = exp$nuclei[[nucleus_id]]$getFieldNameAndIdx()
      }
      return(l)
    },
    setFields = function(){
      fs = .self$getRelateds("fields")
      fields <<- fs$elements
    },
    getKey = function(structures){
      if (length(structures)==1) structures=c(structures, structures)
      key = structures[1]
      if (length(structures>1)) {
        for (i in 2:length(structures)) {
          key=paste(key, i,  sep=";")
        }
      }
      return(key)
    },
    extractStructureData = function(structures,keys,filter=NULL,addIdx=FALSE,useMetaData=TRUE,annotations=TRUE){
      if(!is.fetched) .self$fetchData()
      list.data=list()
      if (is.null(filter)) {
        l = names(.self$nuclei)
      } else {
        l = filter
      }
      for (nucleus_id in l) {
        list.data[[nucleus_id]] = .self$nuclei[[nucleus_id]]$extractStructureData(structures, keys, addIdx,useMetaData,annotations)
      }
      data=rbind.fill(list.data)
      if (!is.null(data) && nrow(data)>0 && annotations) {
        data$experiment.name = .self$getContent("name")
        data$project.name = db
        data$host.name = .self$getHost()
      }
      if (nrow(data)>0 && any(is.na(data))) warning('Missing values in dataframe: either missing keys or wrong number of object')
      return(data)
    },
    extractObjectData = function(structure,keys,filter=NULL,addIdx=FALSE,useMetaData=FALSE,annotations=TRUE){
      if(!is.fetched) .self$fetchData()
      list.data=list()
      if (is.null(filter)) {
        l = names(.self$nuclei)
      } else {
        l = filter
      }
      for (nucleus_id in l) {
        list.data[[nucleus_id]]=.self$nuclei[[nucleus_id]]$extractObjectData(structure, keys, addIdx, useMetaData,annotations)
      }
      data=rbind.fill(list.data)
      if (!is.null(data) && nrow(data)>0 && annotations) {
        data$experiment.name = .self$getContent("name")
        data$project.name = db
        data$host.name = .self$getHost()
      }
      if (is.null(data)) warning('Empty dataframe. You might want to choose different structures or different keys.')
      if (nrow(data)>0 && any(is.na(data))) warning('Missing values in dataframe: either missing keys or wrong number of object')
      return(data)
    },
    getObjectsCount = function(structure,filter=NULL){
      if (is.null(filter)) {
        l = names(.self$nuclei)
      } else {
        l = filter
      }
      countsList=numeric(0)
      for (nucleus_id in l) {
        nucleus = .self$nuclei[[nucleus_id]]
        countsList[nucleus_id] = nucleus$objects3dCount[[structure+1]]
      }
      return(countsList)
    },
    getTagsAndStructures = function(structures=NULL){
      if(!is.fetched) .self$fetchData()
      df = .self$getTags()
      structNames=.self$getAllStructuresNames()
      for (i in 1:length(structNames)) {
        df=cbind(df, .self$getObjectsCount(i-1))
      }
      df=data.frame(df)
      names(df)=c("tags", structNames)
      return(df)
    },
    getFilterFromTags = function(tags=0:10){
      if(is.null(tags)) return(names(nuclei))
      else{
        filter = c()
        info =.self$getTags()
        for(id in names(info)){
          if(info[id] %in% tags) filter = c(filter,id)
        }
        return(filter)
      }
    },
    getFilterFromSelections = function(selections,intersection=FALSE){
      if(is.null(selections)) return(names(nuclei))
      else{
        if(intersection){
          filter = names(nuclei)
          for(selectionName in names(selections)){
            selection = .self$selections[[selectionName]]
            filter = intersect(filter,selection$getFilter())
          }
          return(filter)
        }else{
         filter = c()
         for(selectionName in selections){
           selection = .self$selections[[selectionName]]
           filter = union(filter,selection$getFilter())
         }
         return(filter)
        }
      }
    },
    getFilterFromSelectionsAndTags = function(selections,tags=0:10){
      filter1 = .self$getFilterFromSelections(selections)
      filter2 = .self$getFilterFromTags(tags)
      return(intersect(filter1,filter2))
    },
    getFilter = function(selections, tags=0:10, objectNumber){
      filter1 = .self$getFilterFromSelections(selections)
      filter2 = .self$getFilterFromTags(tags)
      filter3 = .self$getObjectNumberFilter(objectNumber)
      return(intersect(intersect(filter1,filter2),filter3))
    },
    splitDataFrameBySelection = function(dataframe,selection){
      
    },
    getObjectNumberFilter = function(structure.object.number.list) {
      nuc.info=.self$getTagsAndStructures()
      filter = row.names(nuc.info)
      for (structure.name in names(structure.object.number.list)) {
        authorized.numbers = structure.object.number.list[[structure.name]]
        authorized.rows = nuc.info[nuc.info[[structure.name]] %in% authorized.numbers,]
        filter=intersect(filter, row.names(authorized.rows))
      }
      return(filter)
    },
    appendSelections = function(df, selection.names, column.name="population", default.name="no.population", remove.no.population=FALSE) {
      require(plyr)
      appendPopulationName = function(df, column.name, population.name, default.name, sep=".") {
        if (df[1,column.name]==default.name) df[1,column.name]=population.name
        else df[1,column.name]=paste(df[1,column.name], population.name, sep=sep)
        return(df)
      }
      df[,column.name] = default.name
      for (selection in selection.names) {
        if (!is.null(selections[[selection]])) {
          lines = df$nucleus.id%in%selections[[selection]]$getFilter()
          df[lines,]=adply(df[lines,], 1, .fun=appendPopulationName, column.name=column.name, population.name=selection, default.name=default.name)
        } else print(paste("selection:", selection, " not found"))
      }
      if (remove.no.population) df = df[df[,column.name]!=default.name,]
      return(df)
    }
  ),
  contains="mongoDbPersistentObject"
)




