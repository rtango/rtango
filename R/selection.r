selection = setRefClass("selection",
  methods = list(
    setPopulation = function(){
      population <<- "selections"
    },
    setModel = function(){
      model <<- "selection"
    },
    getExperiment = function(){
      return(.self$parents[["experiment"]])
    },
    getStructures = function(){
    },
    getKeys = function(){	
    },
    getFilter = function(){
      return(names(content[["nuclei"]]))
    },
    cleanSiblings = function(){
      buf <- mongo.bson.buffer.create()
      mongo.bson.buffer.append(buf, 'name', content[['name']])
      mongo.bson.buffer.append(buf, 'experiment_id', content[['experiment_id']])
      criteria <- mongo.bson.from.buffer(buf)
      mongo.remove(.self$connector, .self$getNameSpace(), criteria)
      is.saved <<- FALSE
    },
    overwriteSiblings = function(){
      .self$cleanSiblings()
      .self$insert()
    },
    getSiblingsNames = function(){
      xp = .self$getExperiment()
      xp$setSelections()
      return(names(xp$selections))
    },
    checkUniqueName = function(name,siblingsNames){
      if(name %in% siblingsNames) return(FALSE)
      else return(TRUE)
    },
    getUniqueName = function(){
      # il faut fetch les noms des selections a ce moment
      name = content[['name']]
      siblingsNames = .self$getSiblingsNames()
      if(!.self$checkUniqueName(name,siblingsNames)){
        k = 1
        repeat{
          k = k+1
          newname = paste(name,toString(k),sep='_')
          if(.self$checkUniqueName(newname,siblingsNames)) return(newname)
        }
      }else{
        return(name)
      }
    },
    save = function(override=TRUE){
      if(is.null(content[['name']])){
        content[['name']] <<- 'currentSelection'
      }
      if(!override){
        content[['name']] <<- .self$getUniqueName()
      }else{
        .self$cleanSiblings()
      }
      if(!is.saved) .self$insert()
      else mongo.update(connector,.self$getNameSpace(),.self$getSelfQuery(),content)
      xp = .self$getExperiment()
      xp$setSelections()
    },
    toDataFrame = function(){
      straightContent = content
      straightContent['_id'] = mongo.oid.to.string(oid)
      straightContent['experiment_id'] = mongo.oid.to.string(content[['experiment_id']])
      data = data.frame()
      nuclei = .self$getContent('nuclei')
      for(nucleus.id in names(nuclei)){
        df <- rbind.fill(nuclei[[nucleus.id]])
        data <- data.frame(data,df)
      }
      #df["experiment.name"] = .self$getExperiment()$getContent("name")
      #df["project.name"] = db
      return(data)
    },
    unionSelection = function(s){
      l = list(experiment_id=content[['experiment_id']],name=paste(content[['name']],s$content[['name']],sep=' + '),description='',nuclei = list())
      nuclei.ids = union(names(content[['nuclei']]),names(s$content[['nuclei']]))
      for(nucleus.id in nuclei.ids){
        l[['nuclei']][[nucleus.id]] = list()
        nucleus.structures.indexes = union(names(content[['nuclei']][[nucleus.id]]),names(s$content[['nuclei']][[nucleus.id]]))
        for(structure.index in nucleus.structures.indexes){
          l[['nuclei']][[nucleus.id]][[structure.index]] = union(content[['nuclei']][[nucleus.id]][[structure.index]],s$content[['nuclei']][[nucleus.id]][[structure.index]])
        }
      }
      #TODO construction de l'objet selection associé à partir des indexes
      sel=selection$new(connector = connector,content = l,parents = parents,db = db)
      sel$initFields()
      return(sel)
    },
    intersectSelection = function(s){
      #Il faut le même exp_id sinon ça n'a pas de sens?
      l = list(experiment_id=content[['experiment_id']],name=paste(content[['name']],s$content[['name']],sep=' * '),description='',nuclei = list())
      nuclei.ids = intersect(names(content[['nuclei']]),names(s$content[['nuclei']]))
      for(nucleus.id in nuclei.ids){
        l[['nuclei']][[nucleus.id]] = list()
        nucleus.structures.indexes = intersect(names(content[['nuclei']][[nucleus.id]]),names(s$content[['nuclei']][[nucleus.id]]))
        for(structure.index in nucleus.structures.indexes){
          l[['nuclei']][[nucleus.id]][[structure.index]] = intersect(content[['nuclei']][[nucleus.id]][[structure.index]],s$content[['nuclei']][[nucleus.id]][[structure.index]])
        }
      }
      #TODO construction de l'objet selection associé à partir des indexes
      sel=selection$new(connector = connector,content = l,parents = parents,db = db)
      sel$initFields()
      return(sel)
    },
    setdiffSelection = function(s){
      #Il faut le même exp_id sinon ça n'a pas de sens?
      l = list(experiment_id=content[['experiment_id']],name=paste(content[['name']],s$content[['name']],sep=' - '),description='',nuclei = list())
      nuclei.ids = setdiff(names(content[['nuclei']]),names(s$content[['nuclei']]))
      for(nucleus.id in nuclei.ids){
        l[['nuclei']][[nucleus.id]] = list()
        nucleus.structures.indexes = setdiff(names(content[['nuclei']][[nucleus.id]]),names(s$content[['nuclei']][[nucleus.id]]))
        for(structure.index in nucleus.structures.indexes){
          l[['nuclei']][[nucleus.id]][[structure.index]] = setdiff(content[['nuclei']][[nucleus.id]][[structure.index]],s$content[['nuclei']][[nucleus.id]][[structure.index]])
        }
      }
      #TODO construction de l'objet selection associé à partir des indexes
      sel=selection$new(connector = connector,content = l,parents = parents,db = db)
      sel$initFields()
      return(sel)
    },
  	filter = function(df){
      .self$getStructuresNames()
      return()
  	},
    johnnyWeirdFunction = function(s1,s2,n1,n2,tags){
  		return(.self$getExperiment()$getFilter(s1,s2,n1,n2,tags))
  	}#,
#     toDataFrame = function(){
#       .self$fetch()
# 	    exp = .self$getExperiment()
# 	    objectDf = exp$extractObjectData(structure=.self$getStructures(),keys=.self$getKeys(),filter=.self$getFilter(),addIdx=TRUE)
#       structureDf = exp$getExperiment()$extractStructureData(structures=.self$getStructures(),keys=.self$getKeys(),addIdx=TRUE)
#     }                        
  ),
  contains="mongoDbPersistentObject"
)