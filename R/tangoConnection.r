tangoConnection = setRefClass("tangoConnection",
  contains = "mongoDbConnection",
  fields = list(
  	projects = "list"
  ),
  methods = list(
    postInitialize = function(){
      .self$getProjects()
    },
  	getProjects = function(){
  	  l = .self$findInAllTangoDatabases('experiment')
  	  m = list()
  	  for(dbname in names(l)){
  	    n = list()
        xps = l[[dbname]]
        if(length(xps$elements)!=0){
          xpsByName = xps$indexBy('name')
          for(xpname in names(xpsByName)){
  	        xp = xpsByName[[xpname]]
            n[xpname] = xp
  	      }
          projects[[dbname]] <<- n
        }
  	  }
  	},
    extractStructureData = function(structures,keys,addIdx){
      df = data.frame()
      for(projectName in names(workspace)){
        for(experimentName in names(workspace[[projectName]])){
          xp = workspace[[projectName]][[experimentName]]
          df = data.frame(df,xp$extractStructureData(structures,keys,addIdx))
        }
      }
      df["host.name"] = host
      return(df)
    },
    extractObjectData = function(structures,keys,addIdx){
      df = data.frame()
      for(projectName in names(workspace)){
        for(experimentName in names(workspace[[projectName]])){
          xp = workspace[[projectName]][[experimentName]]
          df = data.frame(df,xp$extractObjectData(structures,keys,addIdx))
        }
      }
      df["host.name"] = host
      return(df)
    },
    getExperiments = function(){
      userExperiments = experiments$new(connector=connector,query=mongo.bson.empty(),sort=mongo.bson.empty(),queryfields=mongo.bson.empty())
      userExperiments$fetch()
      return(userExperiments)
    },
    getExperimentsByName = function(){
      xps <- .self$getExperiments()
      return(xps$indexBy("name"))
    },
    getSettingsDatabaseName = function(){
      return(paste("tango",paste(username,"settings",sep="_"),sep="_"))
    },
    getSettingsConnector = function(){
      return(mongo.create(host=host, name="",username=username, password=password, db=.self$getSettingsDatabaseName())) 
    },
  	extractSelections = function(df,selectionName,description=NULL,save=TRUE){
      l = list()
  	  splitDfByProject = split(df, f=df$project.name)
      for(projectName in names(splitDfByProject)){
        m = list()
        projectdf = splitDfByProject[[projectName]]
        splitProjectDfByExperiment = split(projectdf,f=projectdf$experiment.name)
        for(experimentName in names(splitProjectDfByExperiment)){
          experimentdf = splitProjectDfByExperiment[[experimentName]]
          xp = projects[[projectName]][[experimentName]]
          m[[experimentName]] = xp$extractSelection(experimentdf,selectionName=selectionName,description=description,save=save)
        }
        l[[projectName]] = m
      }
      return(l)
  	},
    getTangoDatabases = function(){
      dbs =c()
      for(dbname in mongo.get.databases(connector)){
        if(substr(dbname,1,6)=='tango_') dbs=c(dbs,dbname)
      }
      return(dbs)
    },
    findInAllTangoDatabases = function(collection,query=mongo.bson.empty(),sort=mongo.bson.empty(),queryfields=mongo.bson.empty()){
      l <- list()
      for(dbname in .self$getTangoDatabases()){
        db <<- dbname
        if(paste(dbname,collection,sep='.') %in% mongo.get.database.collections(connector,dbname)){
          founds = .self$find(collection=collection,query = query, sort = sort, queryfields = queryfields)
          l[[dbname]] = founds
        }
      }
      return(l)
    }
  )
)