tangoSession = setRefClass("tangoSession",
  fields = list(
    homepath = "character",
    connections = "list",
    workspaces = "list",
    currentWorkspace = "character",
    configuration = "list",
    data = "list"
  ),
  methods = list(
    initFields = function(){
      .self$createEmptyConfiguration()
      .self$initConnections()
      .self$initWorkspaces()
      .self$initPlots()
    },
    createEmptyConfiguration = function(){
      configuration <<- list(
        homepath = homepath,
        connections = list(),
        workspaces = list()
      )
    },
    addConnection = function(connectionName=NULL,host='localhost',username='',password='',auth=FALSE,db = ''){
      if(is.null(connectionName)) connectionName = host
      configuration[["connections"]][[connectionName]] <<- list(
        host = host,
        username = username,
        password = password,
        auth = auth,
        db = db
      )
      .self$initConnection(connectionName)
    },
    initConnections = function(){
      for(connectionName in names(configuration[["connections"]])){
        .self$initConnection(connectionName)
      }
    },
    initConnection = function(connectionName){
      c = configuration[["connections"]][[connectionName]]
      if(is.null(c[['db']])) c[['db']] = ''
      connections[[connectionName]] <<- tangoConnection$new(
        host = c[['host']],
        username = c[['username']],
        password = c[['password']],
        auth = c[['auth']],
        db = c[['db']]
      )
      connections[[connectionName]]$initFields()
      print('All those projects have been found in this connection:')
      print(names(connections[[connectionName]]$projects))
    },
    addEmptyWorkspace = function(workspaceName){
      configuration[['workspaces']][[workspaceName]] <<- list()
      workspaces[[workspaceName]] <<- list()
    },
    addConnectionToWorkspace = function(workspaceName,connectionName){
      configuration[['workspaces']][[workspaceName]][[connectionName]] <<- list()
      workspaces[[workspaceName]][[connectionName]] <<- list()
    },
    addProjectToWorkspace = function(workspaceName,connectionName,projectName){
      configuration[['workspaces']][[workspaceName]][[connectionName]][[projectName]] <<- c()
      workspaces[[workspaceName]][[connectionName]][[projectName]] <<- list()
    },
    listExperiments = function(workspaceName = 'default',connectionName = 'localhost',projectName){
      return(names(connections[[connectionName]]$projects[[projectName]]))
    },
    addExperimentsToWorkspace = function(workspaceName,connectionName,projectName,experimentsNames){
      configuration[['workspaces']][[workspaceName]][[connectionName]][[projectName]] <<- c(configuration[['workspaces']][[workspaceName]][[connectionName]][[projectName]],experimentsNames)
      xps = c()
      for(experimentName in experimentsNames){
        xp = connections[[connectionName]][['projects']][[projectName]][[experimentName]]
        if(!is.null(xp)){
          xp$fetchData()
          workspaces[[workspaceName]][[connectionName]][[projectName]][[experimentName]] <<- xp
          xps = c(xps,xp)
        }else{
          warning('The experiment does not exists, some project name or experiment name must be wrong')
        }
      }
      return(xps)
    },
    addExperiment = function(workspaceName='default',connectionName=NULL,host='localhost',username='',password='',auth=FALSE,db = '',projectName,experimentName){
      if(is.null(connectionName)) connectionName = host
      .self$addConnection(
        connectionName=connectionName,
        host=host              
      )
      if(is.null(.self$workspaces[[workspaceName]])){
        .self$addEmptyWorkspace(
          workspaceName=workspaceName
        )
      }
      if(is.null(.self$workspaces[[workspaceName]][[connectionName]])){
        .self$addConnectionToWorkspace(
          workspaceName=workspaceName,
          connectionName=connectionName
        )
      }
      if(is.null(.self$workspaces[[workspaceName]][[connectionName]][[projectName]])){
        .self$addProjectToWorkspace(
          workspaceName=workspaceName,
          connectionName=connectionName,
          projectName=projectName
        )
      }
      xp = .self$addExperimentsToWorkspace(
        workspaceName=workspaceName,
        connectionName=connectionName,
        projectName=projectName,
        experimentsNames=experimentName
      )
      return(xp[[1]])
    },
    initWorkspaces = function(){
      for(workspaceName in names(configuration[["workspaces"]])){
        .self$addEmptyWorkspace(workspaceName)
        .self$initWorkspace(workspaceName)
      }
    },
    initWorkspace = function(workspaceName){
      for(connectionName in names(workspaces[[workspaceName]])){
        .self$addConnectionToWorkspace(workspaceName,connectionName)
        for(projectName in names(workspaces[[workspaceName]][[connectionName]])){
          .self$addProjectToWorkspace(workspaceName,connectionName,projectName)
          .self$addExperimentsToWorkspace(workspaceName,connectionName,projectName,workspaces[[workspaceName]][[connectionName]][[projectName]])
        }
      }
    },
    extractSelections = function(df,selectionName,description = NULL,save=TRUE){
      l = list()
      if('host.name' %in% names(df)){
        splitDfByConnection = split(df, f=df$host.name)
        for(hostName in names(splitDfByConnection)){
          m = list()
          hostDf = splitDfByConnection[[hostName]]
          l[[hostName]] = connections[[hostName]]$extractSelections(hostDf,selectionName=selectionName,description=description,save=save)
        }
      }
      return(l)
    },
    extractSelectionsFromWorkFlow = function(workflow,save=TRUE){
      subsets = workflow$getSubsets()
      l = list()
      for(dfName in names(subsets)){
        l[[dfName]] = list()
        for(viewName in names(subsets[[dfName]])){
          if(nrow(subsets[[dfName]][[viewName]])!=0){
            l[[dfName]][[viewName]] = .self$extractSelections(subsets[[dfName]][[viewName]],paste(dfName,viewName,sep='_'),paste(viewName,dfName,sep=' applied to dataframe '),save=save)
          }
        }
      }
      return(l)
    },
    initPlots = function(){
      require(grid)
      require(ggplot2)
      .GlobalEnv$ggplot <- function(...) ggplot2::ggplot(...) + scale_color_brewer(palette='Set1') + scale_fill_brewer(palette='Set1')
      size.factor = 1
      mainsize=12
      tsize2 <- 8 * size.factor
      tsize <- 8 * size.factor
      pchsize <- 1 * size.factor
      lkey <- 3.5 * size.factor # lsize <- 0.3",
      theme_set(theme_bw())
      theme_update( 
        plot.margin = unit(c(0.4,0.5,0.1,0), 'lines'),
        panel.margin = unit(0.25, 'lines'), 
        panel.grid.major =  element_line(colour = 'grey85', size = 0.5),
        panel.grid.minor =  element_line(colour = 'grey90', size = 0.2),
        plot.title = element_text(face='bold', size = mainsize, vjust = 0.5, hjust=0),
        axis.title.x = element_text(size = tsize, vjust = 0.35),
        axis.title.y = element_text(size = tsize, hjust = 0.5, vjust = 0.4, angle = 90),
        axis.text.x = element_text(size = tsize2),
        axis.text.y = element_text(size = tsize2),
        axis.ticks.margin=unit(0.5, units='mm'), 
        axis.ticks.length=unit(1, units='mm'),
        legend.title=element_text(size = tsize2, hjust = 0),
        legend.text=element_text(size = tsize2, hjust = 0),
        legend.background=element_rect(colour=NA, size=0),
        legend.margin = unit(0, 'mm'),
        legend.key.size=unit(lkey,'mm'),
        legend.key.width = unit(lkey * 1.5, 'mm'),
        strip.text.x = element_text(size=tsize, vjust=0.7, hjust= 0.5)
      )
    },
    appendECDF=function(df, measure, split=NULL, xlim=NULL, subtract.unif=FALSE) {
      if (subtract.unif) {
        if (is.null(xlim)) func = function(x) {return(ecdf(x)(x)-punif(min=min(x), max=max(x), x))}
        else func = function(x) {return(ecdf(x)(x)-punif(min=xlim[1], max=xlim[2], x))}
      }
      else func = function(x) {return(ecdf(x)(x))}
      yname = paste(measure, "ecdf", sep=".")
      if (is.null(split)) {
        df$newColumnECDF = func(df[, measure])
      } else {
        df = ddply(df, c(split), .fun=function(x, colName) transform(x, newColumnECDF=func(x[, colName])), colName=measure)
      }
      idx = which(names(df)=="newColumnECDF")
      colnames(df)[idx]=yname
      
      if (!is.null(split)&length(split)==1) { ## adds number of entries
        df = ddply(df, c(split), .fun=function(x, split.name) transform(x, ecdf.split.colname=paste(x[1, split.name], " (N=", nrow(x),")", sep="")), split.name=split)
        color.name=paste(split, "ecdf", sep=".")
        names(df)[which(names(df)=="ecdf.split.colname")] = color.name
      }
      if (subtract.unif) { #summary
        
        print(summaryBy(data=df, as.formula(paste(yname, paste0(split, collapse="+"), sep="~")), FUN=c(max, min, length)))
      }
      return(df)
    },
  plotECDF=function(df, measure, color=NULL, linetype=NULL, size=0.5, xlim=NULL, facet.wrap=NULL) {
    require(ggplot2)
    ggplot <- function(...) ggplot2::ggplot(...) + scale_color_brewer(palette='Set1')
    color.ecdf = paste(color, "ecdf", sep=".")
    if (!is.null(xlim)&&length(xlim)!=2) xlim=c(min(df[,measure]), max(df[,measure]))
    yvar=paste(measure, "ecdf", sep=".")
    split.vars = c(color, linetype, facet.wrap)
    if (length(split.vars)>1|(length(split.vars==1)&is.null(color))) { #  linetype ou linetype&color
      dfPlot = appendECDF(df, measure, split.vars, xlim, FALSE)
      if (is.null(color)) p = ggplot(dfPlot) + geom_step(aes_string(x=measure, y=yvar, linetype=linetype), color=2, size=size) + ylab("")
      else p = ggplot(dfPlot) + geom_step(aes_string(x=measure, y=yvar, color=color, linetype=linetype), size=size) + ylab("")
      p = p+ scale_y_continuous(limits=c(0, 1))
      if (!is.null(facet.wrap)) p = p+  facet_wrap(as.formula(paste("~", facet.wrap, sep="")))
      return(p)
    } else if (!is.null(color)) { ## juste les couleurs
      dfPlot = appendECDF(df, measure, color, xlim, FALSE)
      p = ggplot(dfPlot) + geom_step(aes_string(x=measure, y=yvar, color=color), size=size) + ylab("")
      p = p+ scale_y_continuous(limits=c(0, 1))
      if (!is.null(facet.wrap)) p = p+  facet_wrap(as.formula(paste("~", facet.wrap, sep="")))
      return(p)
    } else { # pas de split
      dfPlot = appendECDF(df, measure, facet.wrap, xlim, FALSE)
      p=ggplot(dfPlot) + geom_step(aes_string(x=measure, y=yvar), color=2, size=size)
      p = p+ scale_y_continuous(limits=c(0, 1))
      return(p)
    }
  },
  mergeDataframeList = function(df.list.list, name, col.name="name", list=NULL) {
    require(plyr)
    df.list = list()
    if (is.null(list)) list = 1:length(df.list.list)
    lev = c()
    for (i in list) {
      n=names(df.list.list)[i]
      lev = c(lev, n)
      df.list[[n]]=df.list.list[[n]][[name]]
      df.list[[n]][[col.name]]=n
    }
    res = rbind.fill(df.list)
    res[, col.name] = factor(res[,col.name], levels = lev) # ordering
    return(res)
  }
  )
)