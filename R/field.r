field = setRefClass("field",
  methods = list(
    setPopulation = function(){
      population <<- "fields"
    },
    setModel = function(){
      model <<- "field"
    },
    "getNuclei" = function(){
      return(.self$getRelateds("nucleui"))
    }
  ),
  contains="mongoDbPersistentObject"
)