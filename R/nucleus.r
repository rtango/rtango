nucleus = setRefClass("nucleus",
  fields = list(
    objects3dCount = "list"
  ),
  methods = list(
    show = function(){
      print("Nucleus are too big to be printed on screen as it contains circular references")
    },
    setPopulation = function(){
      population <<- "nuclei"
    },
    setModel = function(){
      model <<- "nucleus"
    },
    postInitialize = function(){
      .self$setObjects3dCount()
    },
    getField = function(){
      return(.self$parents[["experiment"]]$fields[[as.character(.self$getContent("field_id"))]])
    },
    getFieldName = function(){
      return(.self$getField()$getContent("name"))
    },
    getFieldNameAndIdx = function(){
      return(list(fieldName=.self$getFieldName(),idx=.self$getContent("idx")))
    },
    getExperiment = function(){
        return(.self$parents[["experiment"]])
    },
    getStructures = function(){
      return(.self$getExperiment()$getStructures())
    },
    getAllStructures = function(){
      return(.self$getExperiment()$getAllStructures())
    },
  	getMetaData = function(){
  		return(.self$getExperiment()$metaData)
  	},
    getReverseMetaData = function(){
      m = .self$getMetaData()
      l = list()
      for(name in names(m)){
        idx = as.numeric(m[[name]])
        l[[idx+1]] = name
      }
      return(l)
    },
    getColumnIndex = function(name){
      return(.self$getMetaData()[[name]])
    },
    getColumnsIndexes = function(structuresNames){
      ci = c()
      for(name in structuresNames){
        ci = c(ci,.self$getColumnIndex(name))
      }
      return(ci)
    },
  	getColumnPrefix = function(structure_idx){
  		return(.self$getReverseMetaData()[[structure_idx+1]])
  	},
    getFieldName = function(){
      return(.self$getField()$getContent("name"))
    },
    getIdx = function(){
      return(.self$getContent("idx"))
    },
    getObject3dNameSpace = function(){
      return(paste(db,"object3d",sep="."))
    },
    setObjects3dCount = function(){
      objects3dCount <<- list()
      for(idx in 1:length(.self$getAllStructures())){
        buf <- mongo.bson.buffer.create()
        mongo.bson.buffer.append(buf,"nucleus_id",.self$oid)
        mongo.bson.buffer.append(buf,"channelIdx",idx-1)
        query <- mongo.bson.from.buffer(buf)
        objects3dCount[[idx]] <<- mongo.count(connector,.self$getObject3dNameSpace(),query)
      }
    },
    extractObjectData = function(structure, keys, addIdx=FALSE,useMetaData=FALSE,annotations=FALSE){
      if(is.character(structure)) structure = .self$getColumnsIndexes(structure)
      object3d.namespace = paste(db, "object3d", sep=".")
      nucleus.id = .self$getId()
      fields=list()
      for (key in keys) fields[key]=4L
      list.data=list()
      objectCursor = mongo.find(connector, object3d.namespace, query = list("nucleus_id"=oid, "channelIdx"=structure), sort=list(idx=1L), fields=fields)
      count=1
      while(mongo.cursor.next(objectCursor)) {
        line=mongo.cursor.value(objectCursor)
        line=mongo.bson.to.list(line)
        line[setdiff(keys,names(line))] = NA
        line=data.frame(line[keys])
        list.data[[count]]=line
        count=count+1
      }
      mongo.cursor.destroy(objectCursor)
      data=rbind.fill(list.data)
      if (!is.null(data) && nrow(data)>0) {
        data$nucleus.id=nucleus.id
        if (addIdx) {
	          data$idx=1:nrow(data)
			  if(useMetaData){
				  column.name.new=paste(.self$getColumnPrefix(structure),"idx",sep='.')
				  names(data)[names(data)=="idx"] <- column.name.new
				  newKeys = c("nucleus.id", column.name.new, keys)
			  } else newKeys = c("nucleus.id", "idx", keys)
        } else newKeys = c("nucleus.id", keys)
        if(annotations){
          l=.self$getFieldNameAndIdx()
          data$nucleus.name = paste(l[[1]], l[[2]], sep=".")
          newKeys = c(newKeys,'nucleus.name')
        }
		    if(!is.null(data) && nrow(data)>0){
			    data= subset(data, select=newKeys) #réarrangement des clefs
		    }
      }
      return(data)
    },
    extractStructureData = function(structures,keys,addIdx=FALSE,useMetaData=TRUE,annotations=FALSE){
      structure.measurement.namespace = paste(db, "structureMeasurement", sep=".")
      nucleus.id = .self$getId()
      fields=list()
      buf <- mongo.bson.buffer.create()
      for (key in keys){ 
        mongo.bson.buffer.append(buf, key, 1L)
      }
      queryfields = mongo.bson.from.buffer(buf)
      
      list.data=list()
      if(is.character(structures)) structures = .self$getColumnsIndexes(structures)
      if(length(structures)==1) structures = c(structures, structures)
      
      buf2 <- mongo.bson.buffer.create()
      mongo.bson.buffer.append(buf2, "nucleus_id", oid)
      mongo.bson.buffer.append(buf2, "structures", structures)
      onequery <- mongo.bson.from.buffer(buf2)
      
      data.temp=data.frame()
      data.list=mongo.find.one(connector, structure.measurement.namespace, query=onequery, fields = queryfields)
      if (!is.null(data.list)) {
        data.list=mongo.bson.to.list(data.list)
        if (length(data.list)>1) {
          data.temp=data.frame(data.list[-1]) #creation de la dataframe
          if (nrow(data.temp)>0) {
            data.temp$nucleus.id=nucleus.id #ajout de la colonne nucleus.id
            #ajout des indices
            if (addIdx && length(structures)>1) {
              n1 = objects3dCount[[structures[1]+1]]
              n2 = objects3dCount[[structures[2]+1]]
              if (structures[1]!=structures[2]) {
                if (nrow(data.temp)==n1*n2) {
                  data.temp$idx.1=rep(1:n1, each=n2)
                  data.temp$idx.2=rep(1:n2, times=n1)
                } else {
                  data.temp$idx.1=NA
                  data.temp$idx.2=NA
                }
              } 
              else {
                #matrice triang sup
                if (nrow(data.temp)==n1*(n1-1)/2) {
                  data.temp$idx.1=rep(1:(n1-1), (n1-1):1)
                  idx.2=c()
                  for (i in 2:n1) idx.2=c(idx.2, i:n1)
                  data.temp$idx.2=idx.2
                } else {
                  data.temp$idx.1=NA
                  data.temp$idx.2=NA
                }
              }
			        if(useMetaData){
				        newKeys = c("nucleus.id")
				        for(i in 1:2){
                  column.name=paste('idx',i,sep='.')
					        if(structures[1]==structures[2]) name = column.name
                  else name="idx"
                  column.name.new=paste(.self$getColumnPrefix(structures[i]),name,sep='.')
					        names(data.temp)[names(data.temp)==column.name] <- column.name.new
					        newKeys = c(newKeys,column.name.new)
				        }
				        newKeys = c(newKeys, keys)
			        }
              else newKeys = c("nucleus.id", "idx.1", "idx.2", keys)
            } else newKeys = c("nucleus.id", keys)
            if (annotations){
              l=.self$getFieldNameAndIdx()
              data.temp$nucleus.name = paste(l[[1]], l[[2]], sep=".")
              newKeys = c(newKeys,'nucleus.name')
            }
			      if(!is.null(data.temp) && nrow(data.temp)>0){
				      data.temp[setdiff(keys,names(data.temp))] = NA
				      data.temp = subset(data.temp, select=newKeys) #réarrangement des clés
			      }
          }
        }
      }
      return(data.temp)
    },
    updateTag = function(tag){
      content$tag <<- as.integer(tag)
      mongo.update(connector, paste(db, "nucleus", sep="."), list("_id"=oid), content)
    }
  ),
  contains="mongoDbPersistentObject"
)
