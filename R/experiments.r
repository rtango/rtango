experiments = setRefClass("experiments",
  contains = "mongoDbPersistentPopulation",
  methods = list(
    setPopulation = function(){
      population <<- "experiments"
    },
    setModel = function(){
      model <<- "experiment"
    }
  )
)